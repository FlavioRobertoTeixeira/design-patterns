﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
   

    public enum Formato
    {
        XML,
        CSV,
        PORCENTO
    }

    public class Requisicao
    {
     
        public Formato Formato { get; set; }

        public Requisicao(Formato Formato)
        {
            this.Formato = Formato;
        }

     

    }
}
