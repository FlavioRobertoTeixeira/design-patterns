﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    public class ProcessaRequisicao
    {

        public String processa(Conta conta,Requisicao requisicao)
        {
            IRetornoRequisicao retXml = new RequisicaoXML(requisicao);
            IRetornoRequisicao retCSV = new RequisicaoCSV(requisicao);
            IRetornoRequisicao retPorCento = new RequisicaoPorCento(requisicao);
            IRetornoRequisicao retJson = new RequisicaoJson(requisicao);
            IRetornoRequisicao retNotFound = new SemRequisicao();


            retXml.Proximo = retCSV;
            retCSV.Proximo = retPorCento;
            retPorCento.Proximo =retJson;
            retJson.Proximo = retNotFound;

            return retXml.Retorna(conta);
        }

    }
}
