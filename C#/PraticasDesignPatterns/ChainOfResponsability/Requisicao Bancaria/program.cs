﻿using ChainOfResponsability.OrcamentoExemplo;
using System;


namespace ChainOfResponsability.Requisicao_Bancaria
{
   public class Program
    {
        public static void Main(string[] args)
        {
            Conta conta = new Conta("Flávio", 3000);
            Requisicao requisicao = new Requisicao(Formato.CSV);
            ProcessaRequisicao processador = new ProcessaRequisicao();
            var result = processador.processa(conta, requisicao);
            Console.WriteLine(result);

        }
    }
}
