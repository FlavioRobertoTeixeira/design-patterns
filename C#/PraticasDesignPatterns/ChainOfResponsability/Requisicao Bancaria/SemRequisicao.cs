﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    public class SemRequisicao : IRetornoRequisicao
    {
        public IRetornoRequisicao Proximo { get; set; }

        public string Retorna(Conta conta)
        {
            return null;
        }
    }
}
