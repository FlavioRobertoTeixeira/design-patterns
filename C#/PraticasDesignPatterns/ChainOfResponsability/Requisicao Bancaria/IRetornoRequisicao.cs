﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    public interface IRetornoRequisicao
    {
        string Retorna(Conta conta);

        IRetornoRequisicao Proximo { get; set; }
    }
    
}
