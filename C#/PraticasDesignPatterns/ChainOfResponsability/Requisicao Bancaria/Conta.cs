﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    public class Conta
    {
        public double Saldo { get; set; }
        public string NomeDoTitular { get; set; }

        public Conta(string NomeDoTitular,double Saldo)
        {
            this.Saldo = Saldo;
            this.NomeDoTitular = NomeDoTitular;
        }

    }
}
