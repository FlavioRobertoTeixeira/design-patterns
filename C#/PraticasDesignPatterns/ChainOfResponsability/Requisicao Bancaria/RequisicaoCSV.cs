﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    public class RequisicaoCSV : IRetornoRequisicao
    {
        public IRetornoRequisicao Proximo { get; set; }
        public Requisicao Requisicao { get; set; }


        public RequisicaoCSV(Requisicao requisicao)
        {
            this.Requisicao = requisicao;
        }

       
        public string Retorna(Conta conta)
        {
            if ("CSV".Equals(Requisicao.Formato.ToString()))
            {
                var CSV = "Nome: " + conta.NomeDoTitular + ";" + "Saldo: " + conta.Saldo;
                return CSV;
            }

            return Proximo.Retorna(conta);
        }
    }
}
