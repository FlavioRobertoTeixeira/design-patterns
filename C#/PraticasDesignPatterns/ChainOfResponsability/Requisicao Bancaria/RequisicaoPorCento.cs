﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    public class RequisicaoPorCento : IRetornoRequisicao
    {
        public IRetornoRequisicao Proximo { get; set; }
        public Requisicao Requisicao { get; set; }

        public RequisicaoPorCento(Requisicao Requisicao)
        {
            this.Requisicao = Requisicao;
        }

        public string Retorna(Conta conta)
        {
            if ("PORCENTO".Equals(Requisicao.Formato.ToString()))
            {
                return "";
            }

            return Proximo.Retorna(conta);
        }
    }
}
