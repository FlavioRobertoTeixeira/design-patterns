﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    
    public class RequisicaoXML : IRetornoRequisicao
    {
        public Requisicao Requisicao { get; set; }

        public RequisicaoXML(Requisicao requisicao)
        {
         
            this.Requisicao = requisicao;
        }

        public IRetornoRequisicao Proximo { get; set; }

        public string Retorna(Conta conta){
            if ("XML".Equals(Requisicao.Formato.ToString()))
            {
                string XML = "<Conta>"
                                + "<titular>"
                                    + conta.NomeDoTitular
                                + "</titular>"
                                + "<saldo>"
                                    + conta.Saldo
                                + "</saldo>"
                            + "</Conta>";

                return XML;
            }

            return Proximo.Retorna(conta);
        }

       
    }
}
