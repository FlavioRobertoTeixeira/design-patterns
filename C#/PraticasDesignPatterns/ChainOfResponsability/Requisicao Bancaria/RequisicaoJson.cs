﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.Requisicao_Bancaria
{
    public class RequisicaoJson : IRetornoRequisicao
    {
        public IRetornoRequisicao Proximo { get; set; }
        public Requisicao Requisicao { get; set; }

        public RequisicaoJson(Requisicao Requisicao)
        {
            this.Requisicao = Requisicao;
        }

        public string Retorna(Conta conta)
        {
            if ("JSON".Equals(Requisicao.Formato.ToString()))
            {
                return "{"+"'Nome: '"+conta.NomeDoTitular+","+"'Saldo:'"+conta.Saldo+"}";
            }

            return Proximo.Retorna(conta);
        }
    }
}
