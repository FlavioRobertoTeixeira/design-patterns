﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.OrcamentoExemplo
{
    public class DescontoPorVendaCasada : IDesconto
    {
        public IDesconto Proximo { get ; set ; }

        public double Desconta(Orcamento orcamento)
        {
            if (EhVendaCasada(orcamento))
            {
                return orcamento.Valor * 0.05;
            }

            return Proximo.Desconta(orcamento);
        }

        private bool EhVendaCasada(Orcamento orcamento)
        {
            if (TemItemNoOrcamento("Caneta",orcamento) && TemItemNoOrcamento("Lapis",orcamento))
            {
                return true;
            }

            return false;
        }

        private bool TemItemNoOrcamento(string nome, Orcamento orcamento)
        {
            var item = orcamento.Produtos.Where(x => x.Nome == nome).First();
            if (item != null) {
                return true;
            }
            return false;
        }

        
    }
}
