﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.OrcamentoExemplo
{
    class SemDesconto : IDesconto
    {
        public IDesconto Proximo { get; set ; }

        public double Desconta(Orcamento orcamento)
        {
            return 0;
        }
    }
}
