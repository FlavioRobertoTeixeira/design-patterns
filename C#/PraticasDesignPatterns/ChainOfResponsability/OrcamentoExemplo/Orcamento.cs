﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.OrcamentoExemplo
{
    public class Orcamento
    {
        public double Valor { get; private set; }
        public IList<Produto> Produtos { get; set; }

        public Orcamento(double Valor)
        {
            this.Valor = Valor;
            this.Produtos = new List<Produto>();
        }

        public void Adiciona(Produto produto)
        {
            Produtos.Add(produto);
        }
    }
}
