﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability.OrcamentoExemplo
{
    public class DescontoMaisDe5Produtos : IDesconto
    {
        public IDesconto Proximo { get ; set; }

        public double Desconta(Orcamento orcamento)
        {
            if (orcamento.Produtos.Count > 5)
            {
                return orcamento.Valor * 0.10;
            }

            return Proximo.Desconta(orcamento);
        }
    }
}
