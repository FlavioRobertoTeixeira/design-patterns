﻿using ChainOfResponsability.OrcamentoExemplo;
using System;


namespace ChainOfResponsability
{
    public class Program
    {
        //public static void Main(string[] args)
        //{
        public void Executa()
        {
            Orcamento orcamento = new Orcamento(500);
            orcamento.Adiciona(new Produto("Celular", 100));
            orcamento.Adiciona(new Produto("Balde", 100));
            orcamento.Adiciona(new Produto("Lapis", 100));
            orcamento.Adiciona(new Produto("Caneta", 100));
            orcamento.Adiciona(new Produto("Caderno", 100));

            CalculadorDeDescontos calcDescontos = new CalculadorDeDescontos();
            var descontos = calcDescontos.Calcula(orcamento);
            Console.WriteLine(descontos);
            Console.ReadKey();
        }

        //}
    }
}
