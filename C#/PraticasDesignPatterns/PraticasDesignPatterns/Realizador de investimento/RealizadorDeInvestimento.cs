﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns.Realizador_de_investimento
{
    public class RealizadorDeInvestimento 
    {
        
        public static void Investir(Conta conta,Double valorInvestimento,ITipoInvestimento investimento)
        {
            var valorJuros = investimento.Realiza(valorInvestimento) * 0.75;
            conta.Deposita(valorInvestimento+valorJuros);
            Console.WriteLine(conta.Saldo);
        }
    }
}
