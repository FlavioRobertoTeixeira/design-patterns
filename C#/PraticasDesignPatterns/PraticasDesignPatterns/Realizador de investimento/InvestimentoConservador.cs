﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns.Realizador_de_investimento
{
    public class InvestimentoConservador : ITipoInvestimento
    {
       
        public double Realiza(Double valorInvestimento)
        {
            return valorInvestimento * 0.08;
        }
    }
}
