﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns.Realizador_de_investimento
{
    public class InvestimentoModerado : ITipoInvestimento
    {
        public double Realiza(Double ValorInvestimento)
        {
            if(new Random().Next(10) < 5)
            {
                return ValorInvestimento * 0.025;
            }
            else
            {
                return ValorInvestimento * 0.07;
            }

        }
    }
}
