﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns.Realizador_de_investimento
{
    public interface ITipoInvestimento
    {
        Double Realiza(Double ValorInvestimento);
    }
}
