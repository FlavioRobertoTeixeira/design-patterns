﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns.Realizador_de_investimento
{
    public class InvestimentoArrojado : ITipoInvestimento
    {
        public double Realiza(double ValorInvestimento)
        {
            var random =  new Random().Next(10) ;
            if (random <= 2)
            {
                return 0.05 * ValorInvestimento;
            }

            if (random <= 3)
            {
                return 0.03 * ValorInvestimento;
            }

            if (random >= 5)
            {
                return 0.06 * ValorInvestimento;
            }

            return 0;
        }
    }
}
