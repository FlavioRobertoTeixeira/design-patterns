﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns.Realizador_de_investimento
{
    public class Conta
    {
        public Double Saldo { get; private set; }

        public Conta(Double Saldo)
        {
            this.Saldo = Saldo;
        }

        public Double Deposita(Double valor)
        {
            Saldo += valor;
            return Saldo;
        }
    }
}
