﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns
{
    public class Orcamento
    {
        public Double Valor { get; private set; }

        public Orcamento(Double valor)
        {
            this.Valor = valor;
        }
    }
}
