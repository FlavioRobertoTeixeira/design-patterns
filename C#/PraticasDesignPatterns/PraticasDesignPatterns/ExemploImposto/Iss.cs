﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns
{
    public class Iss : Iimposto
    {
        public double Calcula(Orcamento orcamento)
        { 
            return orcamento.Valor * 0.15;   
        }
    }
}
