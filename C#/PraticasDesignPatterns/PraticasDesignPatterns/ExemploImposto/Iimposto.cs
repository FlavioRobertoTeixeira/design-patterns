﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns
{
    public interface Iimposto
    {
        Double Calcula(Orcamento orcamento);
    }
}
