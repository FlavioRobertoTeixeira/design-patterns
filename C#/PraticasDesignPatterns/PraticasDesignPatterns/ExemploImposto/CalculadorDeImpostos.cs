﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns
{
   public class CalculadorDeImpostos
    {

        public static void RealizaCalculo(Orcamento orcamento, Iimposto Imposto)
        {
           var valor = Imposto.Calcula(orcamento);
            Console.WriteLine(valor);
        }

    }
}
