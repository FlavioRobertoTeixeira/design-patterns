﻿using PraticasDesignPatterns.Realizador_de_investimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraticasDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            RealizadorDeInvestimento.Investir(new Conta(0), 1200, new InvestimentoModerado());
            RealizadorDeInvestimento.Investir(new Conta(0), 1200, new InvestimentoArrojado());
            RealizadorDeInvestimento.Investir(new Conta(0), 1200, new InvestimentoConservador());
            Console.ReadKey();

        }
    }
}
